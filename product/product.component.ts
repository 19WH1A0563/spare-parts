// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { CartService } from '../cart.service';

// @Component({
//   selector: 'app-product',
//   templateUrl: './product.component.html',
//   styleUrls: ['./product.component.css']
// })
// export class ProductComponent implements OnInit {
//   parts : any;
//   item_qty : any;
//   part : any;
//   itemExists : any;
//   public totalItem : number=0;

//   constructor(private router:Router,private service:CartService) {
//     this.parts = [
//       {partId:1,  partName:'Laser fog light',        price:450.00,  imagePath:'/assets/images/1.jpeg', ret:6},
//     //   {partId:2,  partName:'GenericU5 Bike Projector',        price:400.00,  imagePath:'src\assets\images\2.jpeg', ret:6},
//     //   {partId:3,  partName:'',        price:690.00,  imagePath:'src\assets\images\3.jpeg', ret:6},
//     //   {partId:4,  partName:'APPLE',        price:810.00,  imagePath:'src\assets\images\4.jpeg', ret:6},
//     //   {partId:5,  partName:'APPLE',        price:730.00,  imagePath:'src\assets\images\5.jpeg', ret:6},
//     //   {partId:6,  partName:'APPLE',        price:910.00,  imagePath:'src\assets\images\6.jpeg', ret:6},
//     //   {partId:7,  partName:'APPLE',        price:720.00,  imagePath:'src\assets\images\7.jpeg', ret:6},
//     //   {partId:8,  partName:'APPLE',        price:680.00,  imagePath:'src\assets\images\8.jpeg', ret:6},
//     //   {partId:9,  partName:'APPLE', {partId:1        price:810.00,  imagePath:'src\assets\images\9.jpeg', ret:6},
//     //  0,  partName:'APPLE',        price:710.00,  imagePath:'src\assets\images\10.jpeg', ret:6},
     
    


//     ];
//    }

//   ngOnInit(): void {
//     this.service.getParts()
//     .subscribe((res: string | any[])=>{
//       this.totalItem = res.length;
//     })
//   }
//   addToCart(part: any){
//     this.service.addtoCart(part);
//   }
//   btnAddToCart(part: any) {
//     this.service.addtoCart(part);
//       if (part.partId) {
//         this.itemExists = part.partId;
//       }
//     }
// }


// import { Component, OnInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/cart.service';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
    product : any;
    item_qty : any;
    item : any;
    public totalItem : number = 0;
  constructor(private router:Router,private service:CartService) {
    this.product = [
      // {name:'BOAT-ROCKERS',description:'BOAT-ROCKERS',quantity:1,price:1500,imagePath:'/assets/images/BOAT-ROCKERS.jpg'},
   {name:'DUGGLE BAG',quantity:1,price:1200,imagePath:'/assets/images/1.jpeg'},
   {name:'GENRICU5 BIKE',quantity:1,price:1000,imagePath:'/assets/images/2.jpeg'},
   {name:'AUTO FETCH',quantity:1,price:500,imagePath:'/assets/images/3.jpeg'},
   {name:'AUTO FETCH WIPER',quantity:1,price:989,imagePath:'/assets/images/4.jpeg'},
   {name:'CYD LED BIKE',quantity:1,price:1100,imagePath:'/assets/images/5.jpeg'},
   {name:'12V CAR LED STRIP LIGHT',quantity:1,price:1099,imagePath:'/assets/images/6.jpeg'},
   {name:'SINGLE RADIATOR',quantity:1,price:1,imagePath:'/assets/images/7.jpeg'},
   {name:'DRL ULTRA COB CAR WIRE',quantity:1,price:1499,imagePath:'/assets/images/8.jpeg'},
   {name:'CAR ENGINE',quantity:1,price:2000,imagePath:'/assets/images/9.jpg'},

   

  ]
   }

  ngOnInit(): void {
    this.service.getProducts()
    .subscribe((res: string | any[])=>{
      this.totalItem = res.length;
    })
  }
  addtocart(item: any){
    this.service.addtoCart(item);
}
  
}