import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductComponent } from './product/product.component';
import { CartComponent } from './cart/cart.component';
import {HeaderComponent} from './header/header.component';
import { PaymentComponent } from './payment/payment.component';
import { EndComponent } from './end/end.component';

const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'product',component:ProductComponent},
  {path:'cart',component:CartComponent},
  {path:'header', component:HeaderComponent},
  {path:'payment', component:PaymentComponent },
  {path:'end', component:EndComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
