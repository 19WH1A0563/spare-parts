// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-cart',
//   templateUrl: './cart.component.html',
//   styleUrls: ['./cart.component.css']
// })
// export class CartComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }





import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public products : any = [];
  public grandTotal !: number;
  constructor(private service: CartService) { }

  ngOnInit(): void {
    this.service.getProducts()
    .subscribe(res=>{
    this.products = res;
    this.grandTotal = this.service.getTotalPrice();
  })

}
removeItem(item: any){
  this.service.removeCartItem(item);
}
emptycart(){
  this.service.removeAllCart();
}
}
